#include <SoftwareSerial.h>
#define TX 10
#define RX 11
SoftwareSerial Blue(TX, RX);

// mai adauv servo :)
// poate mai adaug si ultrasunete

float tensiune = 0;
char state;
unsigned long vSpeed = 200, aux;
unsigned long initial = 0;

const int analogIn = A0; // Pin conectat la divizorul de tensiune
const int motorA1  = 9;  // Pin  2 al L293
const int motorA2  = 3;  // Pin  7 al L293
const int motorB1  = 6; // Pin 10 al L293
const int motorB2  = 5;  // Pin 14 al L293

const int lightsF = 7; //limini fata
const int lightsB = 4;  //lumini spate
const int horn = 8;    // claxon

const int batOk = 2; // indicator baterie- OK;
const int batNOk = 12; // indicator baterie- Descarcata;

void setup()
{
  Serial.begin(9600);
  Blue.begin(9600);
  Blue.setTimeout(10000);

  pinMode(motorA1, OUTPUT);
  pinMode(motorA2, OUTPUT);
  pinMode(motorB1, OUTPUT);
  pinMode(motorB2, OUTPUT);
  pinMode(lightsF, OUTPUT);
  pinMode(lightsB, OUTPUT);
  pinMode(horn, OUTPUT);

  pinMode(batOk, OUTPUT);
  pinMode(batNOk, OUTPUT);
  pinMode(analogIn, INPUT);

}


void loop()
{
  //Serial.println(state);
  tensiune = analogRead(analogIn);
  float valTensiune = (8.1  * tensiune) / 1023.0;
    

  if (valTensiune <= 6)
  {
    digitalWrite(batNOk, HIGH);
    digitalWrite(batOk,LOW);
  }
  else if(valTensiune >=7)
  {
    digitalWrite(batOk,HIGH);
    digitalWrite(batNOk, LOW);
  }

   // delay(50);
  // initial = millis();
  if (Blue.available() > 0)
  {
    state = Blue.read();
    /*Blue.print("State: "); Blue.println(state);
    Serial.print("State: "); Serial.println(state);
    Serial.print("vSpeed: "); Serial.println(vSpeed);
    Serial.print("valTensiune: "); Serial.println(valTensiune);*/
  }


  // reglaj viteza :)

  if (state == '0') {
    vSpeed = 90; //22.5  dar pt ca la 7.5 V porneste de la 90 am 15
  }
  else if (state == '1') {
    vSpeed = 105;
  }
  else if (state == '2') {
    vSpeed = 120;
  }
  else if (state == '3') {
    vSpeed = 135;
  }
  else if (state == '4') {
    vSpeed = 150;
  }
  else if (state == '5') {
    vSpeed = 165;
  }
  else if (state == '6') {
    vSpeed = 180;
  }
  else if (state == '7') {
    vSpeed = 195;
  }
  else if (state == '8') {
    vSpeed = 210;
  }
  else if (state == '9') {
    vSpeed = 225;
  }
  else if (state == 'q') {
    vSpeed = 255;
  }


  /***********************Inainte****************************/
  //Daca state este egal cu litera 'F', masina merge inainte
  if (state == 'F') {
    analogWrite(motorA1, vSpeed); analogWrite(motorA2, 0);
    analogWrite(motorB1, vSpeed);      analogWrite(motorB2, 0);
  }
  /*********************Inainte stanga************************/
  //Daca state este egal cu litera 'G', masina merge inainte stanga
  else if (state == 'G') {
    analogWrite(motorA1, vSpeed); analogWrite(motorA2, 0);
    analogWrite(motorB1, 90);    analogWrite(motorB2, 0);
  }
  /**********************Inaint dreapta************************/
  //Daca state este egal cu litera 'I', masina merge inainte dreapta
  else if (state == 'I') {
    analogWrite(motorA1, 90); analogWrite(motorA2, 0);
    analogWrite(motorB1, vSpeed);      analogWrite(motorB2, 0);
  }
  /***********************Inapoi****************************/
  //Daca state este egal cu litera 'B', masina merge inapoi
  else if (state == 'B') {
    analogWrite(motorA1, 0);   analogWrite(motorA2, vSpeed);
    analogWrite(motorB1, 0);   analogWrite(motorB2, vSpeed);
  }
  /**********************Inapoi stanga************************/
  //Daca state este egal cu litera 'H', masina merge inapoi stanga
  else if (state == 'H') {
    analogWrite(motorA1, 0);   analogWrite(motorA2, vSpeed);
    analogWrite(motorB1, 0);   analogWrite(motorB2, 90);
  }
  /**********************Inapoi dreapta************************/
  //Daca state este egal cu litera 'J', masina merge inapoi dreapta
  else if (state == 'J') {
    analogWrite(motorA1, 0);   analogWrite(motorA2, 90);
    analogWrite(motorB1, 0);   analogWrite(motorB2, vSpeed);
  }
  /***************************Stanga*****************************/
  //Daca state este egal cu litera 'L', masina merge stanga
  else if (state == 'L') {
    analogWrite(motorA1, 0);   analogWrite(motorA2, 90);
    analogWrite(motorB1, 200); analogWrite(motorB2, 0);
  }
  /***************************Dreapta*****************************/
  //Daca state este egal cu litera 'R', masina merge dreapta
  else if (state == 'R') {
    analogWrite(motorA1, 0);   analogWrite(motorA2, 200);
    analogWrite(motorB1, 90);   analogWrite(motorB2, 0);
  }
  else if (state == 'W') // Lumini fata ON
  {
    digitalWrite(lightsF, HIGH);
  }
  else if (state == 'w') //lumin fata OFF
  {
    digitalWrite(lightsF, LOW);
  }
  else if (state == 'U') // Lumini Spate ON
  {
    digitalWrite(lightsB, HIGH);
  }
  else if (state == 'u') // lumini spate off
  {
    digitalWrite(lightsB, LOW);
  }
  else if (state == 'V') //Claxon ON
  {
    tone(horn, 1000);//Speaker on
  }
  else if (state == 'v') //Claxon off
  {
    noTone(horn);
  }
  else if (state == 'X') //avarii ON
  {
    initial++;
    if (initial == 20000)
    {
      digitalWrite(lightsF, HIGH);
      digitalWrite(lightsB, HIGH);
      tone(horn, 1000);//Speaker on
    }
    else if (initial == 40000)
    {
      initial = 0;
      digitalWrite(lightsF, LOW);
      digitalWrite(lightsB, LOW);
      noTone(horn);
    }

  }
  else if (state ==  'x') //avarii off
  {
    digitalWrite(lightsF, LOW);
    digitalWrite(lightsB, LOW);
    noTone(horn);
  }

  else if (state == 'S') //Masina oprita
  {
    analogWrite(motorA1, 0); analogWrite(motorB1, 0);
    analogWrite(motorA2, 0); analogWrite(motorB2, 0);
    //state = 'S';
  }

  else if (state == 'D') // Masina stationeaza
  {
    digitalWrite(lightsF, LOW);
    digitalWrite(lightsB, LOW);
    noTone(horn);
    analogWrite(motorA1, 0); analogWrite(motorB1, 0);
    analogWrite(motorA2, 0); analogWrite(motorB2, 0);
  }
  else
  {
    state = 'S';
  }

}

//digitalWrite(6, LOW);
// digitalWrite(7, LOW);

