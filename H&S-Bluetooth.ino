#include <CurieBLE.h>
#include <CurieIMU.h>
#include <MadgwickAHRS.h>
#include "DHT.h"

// Create my own UUIDs; used https://www.uuidgenerator.net/
#define IMU_SERVICE_UUID "2947ac9e-fc38-11e5-86aa-5e5517507c66"
#define AX_CHAR_UUID "2947af14-fc38-11e5-86aa-5e5517507c66"
#define AY_CHAR_UUID "2947b090-fc38-11e5-86aa-5e5517507c66"
#define AZ_CHAR_UUID "2947b180-fc38-11e5-86aa-5e5517507c66"
#define GX_CHAR_UUID "2947b252-fc38-11e5-86aa-5e5517507c66"
#define GY_CHAR_UUID "2947b5ae-fc38-11e5-86aa-5e5517507c66"
#define GZ_CHAR_UUID "2947b694-fc38-11e5-86aa-5e5517507c66"
//#define BATTERY_VOLTAGE_UUID "971fbb2a-cb96-46ed-b174-85838e6dda35"
#define BATTERY_VOLTAGE_UUID "29477abc-fc38-11e5-86aa-5e5517507c66"

#define EDISON_SERVICE_UUID "8bce1975-31f5-4e19-a3e0-f30016004f3d"
#define ROTATION_CHAR_UUID "8bce2020-31f5-4e19-a3e0-f30016004f3d"

#define DHTPIN 2
#define DHTTYPE DHT11   // DHT 22  (AM2302), AM2321
//DHT initialization
DHT dht(DHTPIN, DHTTYPE);

// Arduino 101 acts as a BLE
BLEPeripheral blePeripheral;

// IMU data is registered as a BLE service
BLEService imuService(IMU_SERVICE_UUID);
BLEService edisonService(EDISON_SERVICE_UUID);

//Serviciul nostru


// Each IMU data point is its own characteristic
BLEIntCharacteristic gxChar(GX_CHAR_UUID, BLERead | BLENotify);
BLEIntCharacteristic gyChar(GY_CHAR_UUID, BLERead | BLENotify);
BLEIntCharacteristic gzChar(GZ_CHAR_UUID, BLERead | BLENotify);
BLEIntCharacteristic axChar(AX_CHAR_UUID, BLERead | BLENotify);
BLEIntCharacteristic ayChar(AY_CHAR_UUID, BLERead | BLENotify);
BLEIntCharacteristic azChar(AZ_CHAR_UUID, BLERead | BLENotify);
BLEIntCharacteristic batteryLevelChar(BATTERY_VOLTAGE_UUID, BLERead | BLENotify | BLEWrite);

// edisonService chars
BLEIntCharacteristic rotationsChar(ROTATION_CHAR_UUID, BLENotify | BLEWrite);

// Assign pin to indicate BLE connection
const int INDICATOR_PIN = 13;

int ax = 0;
int ay = 0;
int az = 0;
int gx = 0;
int gy = 0;
int gz = 0;
int batteryVoltage = 0;
int batteryLevel = 0;
int rotations = 0;

float h , t;
float hif;

long previousMillis = 0;

void setup() {
  Serial.begin(9600);

  dht.begin();
  Serial.println("DHT11 Started!");

  // Initialize IMU
  Serial.println("Initializing IMU...");
  CurieIMU.begin();
  CurieIMU.autoCalibrateGyroOffset();
  CurieIMU.autoCalibrateAccelerometerOffset(X_AXIS, 0);
  CurieIMU.autoCalibrateAccelerometerOffset(Y_AXIS, 0);
  CurieIMU.autoCalibrateAccelerometerOffset(Z_AXIS, 1);

  // Now, activate the BLE



  // Initialize BLE
  blePeripheral.setLocalName("status2");
  blePeripheral.setAdvertisedServiceUuid(imuService.uuid());


  imuService.addCharacteristic(gxChar);
  imuService.addCharacteristic(gyChar);
  imuService.addCharacteristic(gzChar);
  imuService.addCharacteristic(axChar);
  imuService.addCharacteristic(ayChar);
  imuService.addCharacteristic(azChar);
  imuService.addCharacteristic(batteryLevelChar);
  blePeripheral.addAttribute(imuService);


  // edisonService.addCharacteristic(rotationsChar);
  //    blePeripheral.addAttribute(edisonService);


  //ble.setAdvertisedServiceUuid(edisonService.uuid());
  //  ble.addAttribute(imuService);
  //  ble.addAttribute(axChar);
  //  ble.addAttribute(ayChar);
  //  ble.addAttribute(azChar);
  //  ble.addAttribute(gxChar);
  //  ble.addAttribute(gyChar);
  //  ble.addAttribute(gzChar);
  //  ble.addAttribute(batteryLevelChar);



  // ble.addAttribute(edisonService);
  // ble.addAttribute(rotationsChar);

  // Set initial values
  axChar.setValue(ax);
  ayChar.setValue(ay);
  azChar.setValue(az);
  gxChar.setValue(gx);
  gyChar.setValue(gy);
  gzChar.setValue(gz);
  //batteryLevelChar.setValue(batteryLevel);
  rotationsChar.setValue(rotations);

  // start advertising
  //BLE.advertise();
  Serial.println("BEfore   BLE.begin();");
  blePeripheral.begin();

}

void loop() {
  // Check if the connection to the central is active or not
  Serial.println("start loop" );

  BLECentral central = blePeripheral.central();
  //central.poll();

  // h = dht.readHumidity();
  //t = dht.readTemperature();


  if (central) {
    Serial.print("Connected to central: ");
    Serial.println(central.address());
    digitalWrite(INDICATOR_PIN, HIGH);

    while (central.connected())
    {
      //Serial.println("Before scan");
      //BLE.scanForUuid("73d4c498-1103-4e6b-86a7-715792adb1fe");
      //BLE.scan();
      delay(2000);
      BLEDevice peripheral = BLE.available();
      if (peripheral)
      {
        // discovered a peripheral, print out address, local name, and advertised service
        Serial.print("Found ");
        Serial.print(peripheral.address());
        Serial.print(" '");
        Serial.print(peripheral.localName());
        Serial.print("' ");
        Serial.print(peripheral.advertisedServiceUuid());
        Serial.println();

        //controlLed(peripheral);
      }
      // stop scanning
      //BLE.stopScan();
      updateImuData();
    }

    Serial.print("Disconnected from central: ");
    Serial.println(central.address());
    digitalWrite(INDICATOR_PIN, LOW);
  }
}

void updateImuData() {
  CurieIMU.readMotionSensor(ax, ay, az, gx, gy, gz);

  axChar.setValue(ax);
  ayChar.setValue(ay);
  azChar.setValue(az);
  gxChar.setValue(gx);
  gyChar.setValue(gy);
  gzChar.setValue(gz);
  //batteryLevelChar.setValue(batteryLevel);
  batteryVoltage = analogRead(A0);
  batteryLevel = map(batteryVoltage, 0, 1023, 0, 100);

  h = dht.readHumidity();
  t = dht.readTemperature();
  if (isnan(h) || isnan(t))
  {
    Serial.println("Failed to read from DHT sensor!");
  }
  hif = dht.computeHeatIndex(t, h, false);

  Serial.print(ax); Serial.print("\t");
  Serial.print(ay); Serial.print("\t");
  Serial.print(az); Serial.print("\t");
  Serial.print(gx); Serial.print("\t");
  Serial.print(gy); Serial.print("\t");
  Serial.print(gz); Serial.println("\t");

  Serial.print("Batery level: ");
  Serial.println(batteryLevel);

  Serial.print("Batery level written from Edison: ");
  Serial.println(batteryLevelChar.value());

  Serial.print("Roations written from Edison: ");
  Serial.println(rotationsChar.value());


  Serial.print("Humidity: ");
  Serial.print(h);
  Serial.print(" %\t");
  Serial.print("Temperature: ");
  Serial.print(t);
  Serial.print(" *C ");
  Serial.print(hif);
  Serial.println(" *C");
}
